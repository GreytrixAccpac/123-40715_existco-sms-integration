﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Existco_OEOrderShipmemtInvoiceCrNotes
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //args = new string[4];
            //args[0] = "WHTRN1";
            //args[1] = "TEST";
            //args[2] = "Test1";
            //args[3] = "1"; //1 from Desktop and 0 from task scheduler
            if (args.Length == 4)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmImportState(args[0], args[1], args[2],args[3]));
            }
            else
            {
                System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Insufficient arguments passed.");
               
            }

        }
    }
}
