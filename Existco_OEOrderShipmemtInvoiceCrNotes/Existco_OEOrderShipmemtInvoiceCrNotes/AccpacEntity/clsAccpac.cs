﻿using System;
using Existco_OEOrderShipmemtInvoiceCrNotes.LogFileEntity;
using System.Data;
using AccpacCOMAPI;
using AccpacSessionManager;
using System.IO;

namespace Existco_OEOrderShipmemtInvoiceCrNotes.AccpacEntities
{

    class clsAccpac
    {
        #region "AccpacSessionObjects"
        public AccpacSession pSession;
        public AccpacDBLink mDBLinkCmpRE;
        public AccpacDBLink mDBLinkCmpRW;
        private AccpacSessionMgr sessmgr;
        public AccpacView objCSQry;
        private const string CSVIEW = "CS0120";
        #endregion "AccpacSessionObjects"

        #region "Common Variables"
        public string str_value = "";
        public string str_StdError = "";
        #endregion "Common Variables"

        #region "SessionInfoVariables"
        string APP_ID = "";
        string ProgramName = "";
        string Sage300Version = "";
        string Sage300User = "";
        string Sage300Password = "";
        string Sage300CmpDBID = "";

        public clsAccpac(string str_APP_ID, string str_ProgramName, string str_Sage300Version, string str_Sage300User, string str_Sage300Password, string str_Sage300CmpDBID, ref LogFile logFile)
        {
            APP_ID = str_APP_ID;
            ProgramName = str_ProgramName;
            Sage300Version = str_Sage300Version;
            Sage300User = str_Sage300User;
            Sage300Password = str_Sage300Password;
            Sage300CmpDBID = str_Sage300CmpDBID;
            objLogFile = logFile;
        }


        #endregion "SessionInfoVariables"

        #region "LogFile"
        private LogFile objLogFile;
        #endregion "LogFile"

        #region "Accpac Views"

        #region CreditNote Views
        public AccpacCOMAPI.AccpacView OECRD1header;
        public AccpacCOMAPI.AccpacViewFields OECRD1headerFields;
        public AccpacCOMAPI.AccpacView OECRD1detail1;
        public AccpacCOMAPI.AccpacViewFields OECRD1detail1Fields;
        AccpacCOMAPI.AccpacView OECRD1detail2;
        AccpacCOMAPI.AccpacViewFields OECRD1detail2Fields;
        AccpacCOMAPI.AccpacView OECRD1detail3;
        AccpacCOMAPI.AccpacViewFields OECRD1detail3Fields;
        AccpacCOMAPI.AccpacView OECRD1detail4;
        AccpacCOMAPI.AccpacViewFields OECRD1detail4Fields;
        AccpacCOMAPI.AccpacView OECRD1detail5;
        AccpacCOMAPI.AccpacViewFields OECRD1detail5Fields;
        AccpacCOMAPI.AccpacView OECRD1detail6;
        AccpacCOMAPI.AccpacViewFields OECRD1detail6Fields;
        AccpacCOMAPI.AccpacView OECRD1detail7;
        AccpacCOMAPI.AccpacViewFields OECRD1detail7Fields;
        AccpacCOMAPI.AccpacView OECRD1detail8;
        AccpacCOMAPI.AccpacViewFields OECRD1detail8Fields;
        AccpacCOMAPI.AccpacView OECRD1detail9;
        AccpacCOMAPI.AccpacViewFields OECRD1detail9Fields;
        AccpacCOMAPI.AccpacView OECRD1detail10;
        AccpacCOMAPI.AccpacViewFields OECRD1detail10Fields;
        #endregion CreditNote Views

        #region OEInvoice Views
        public AccpacCOMAPI.AccpacView OEORD1header;
        public AccpacCOMAPI.AccpacViewFields OEORD1headerFields;

        public AccpacCOMAPI.AccpacView OEORD1detail1;
        public AccpacCOMAPI.AccpacViewFields OEORD1detail1Fields;

        AccpacCOMAPI.AccpacView OEORD1detail2;
        AccpacCOMAPI.AccpacViewFields OEORD1detail2Fields;

        AccpacCOMAPI.AccpacView OEORD1detail3;
        AccpacCOMAPI.AccpacViewFields OEORD1detail3Fields;

        AccpacCOMAPI.AccpacView OEORD1detail4;
        AccpacCOMAPI.AccpacViewFields OEORD1detail4Fields;

        AccpacCOMAPI.AccpacView OEORD1detail5;
        AccpacCOMAPI.AccpacViewFields OEORD1detail5Fields;

        AccpacCOMAPI.AccpacView OEORD1detail6;
        AccpacCOMAPI.AccpacViewFields OEORD1detail6Fields;

        AccpacCOMAPI.AccpacView OEORD1detail7;
        AccpacCOMAPI.AccpacViewFields OEORD1detail7Fields;

        AccpacCOMAPI.AccpacView OEORD1detail8;
        AccpacCOMAPI.AccpacViewFields OEORD1detail8Fields;

        AccpacCOMAPI.AccpacView OEORD1detail9;
        AccpacCOMAPI.AccpacViewFields OEORD1detail9Fields;

        AccpacCOMAPI.AccpacView OEORD1detail10;
        AccpacCOMAPI.AccpacViewFields OEORD1detail10Fields;

        AccpacCOMAPI.AccpacView OEORD1detail11;
        AccpacCOMAPI.AccpacViewFields OEORD1detail11Fields;

        AccpacCOMAPI.AccpacView OEORD1detail12;
        AccpacCOMAPI.AccpacViewFields OEORD1detail12Fields;
        #endregion OEInvoice Views

        #endregion "Accpac Views"

        #region "Accpac Create/Close Session"
        /// <summary>
        /// opens Sage 300 Lanpack connection 
        /// </summary>
        /// <returns></returns>
        public bool CreateAccpacSession()
        {
            try
            {

                if (APP_ID != "")
                {
                    pSession = new AccpacCOMAPI.AccpacSession();
                    //objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection1", "SUCCESS", "APP_ID = " + APP_ID + " Program Name " + ProgramName + " Sage300Version =" + Sage300Version + "Sage 300 User : " + Sage300User + " Sage300Password: " + Sage300Password);
                    pSession.Init("", APP_ID, ProgramName, Sage300Version);
                    //objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection2", "SUCCESS","");
                    pSession.Open(Sage300User.ToUpper(), Sage300Password.ToUpper(), Sage300CmpDBID, DateTime.Now, 0, "");
                    //objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection3", "SUCCESS", "");
                    mDBLinkCmpRW = pSession.OpenDBLink(AccpacCOMAPI.tagDBLinkTypeEnum.DBLINK_COMPANY, AccpacCOMAPI.tagDBLinkFlagsEnum.DBLINK_FLG_READWRITE);
                    mDBLinkCmpRE = pSession.OpenDBLink(AccpacCOMAPI.tagDBLinkTypeEnum.DBLINK_COMPANY, AccpacCOMAPI.tagDBLinkFlagsEnum.DBLINK_FLG_READONLY);
                    objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection", "SUCCESS", "Successfully created session for Sage 300 Company " + Sage300CmpDBID);

                    return true;
                }
                else
                { return false; }
            }
            catch(Exception ex)
            {
                objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection", "FAIL", ex.Message + "\nFail to create session for Sage 300 company " + Sage300CmpDBID);
                return false;
            }
            finally { GC.Collect(); }
        }

        //S:GT:KIRT:06262018: Code to Close all Connections.
        public void CloseAccpacConnection()
        {
            try
            {
                mDBLinkCmpRE.Close();
                mDBLinkCmpRW.Close();
                if (objCSQry != null)
                    objCSQry.Close();
            }
            catch
            {
                str_StdError = GetAdvError();
                pSession.Errors.Clear();
            }
            finally { GC.Collect(); }
        }
        //E:GT:KIRT:06262018: Code to Close all Connections.
        #endregion "Accpac /Close Session"

        #region "Connect Existing Session"
        /// <summary>
        /// uses existing Sage 300 Lanpack connection 
        /// </summary>
        /// <returns></returns>
        private bool ConnectSage300()
        {
            try
            {
                int lSignonID = 0;
                sessmgr = new AccpacSessionManager.AccpacSessionMgr();
                sessmgr.AppID = APP_ID;
                sessmgr.ProgramName = ProgramName;
                sessmgr.ServerName = "";
                sessmgr.AppVersion = Sage300Version;
                sessmgr.CreateSession("", ref lSignonID, out pSession);
                mDBLinkCmpRE = pSession.OpenDBLink(AccpacCOMAPI.tagDBLinkTypeEnum.DBLINK_COMPANY, AccpacCOMAPI.tagDBLinkFlagsEnum.DBLINK_FLG_READONLY);
                pSession = mDBLinkCmpRE.Session;
                //objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection", "SUCCESS", "APP_ID = " + APP_ID + " Program Name " + ProgramName + " Sage300Version =" + Sage300Version);
                objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection", "SUCCESS", "Successfully accessed existing Sage 300 session for Company " + pSession.CompanyID);
                return true;
            }
            catch
            {
                objLogFile.LogWrite(DateTime.Now.ToShortDateString(), "Accpac Connection", "FAIL", "Fail to access existing Sage 300 session for Company " + Sage300CmpDBID);
                return false;
            }
        }
        #endregion "Connect Existing Session"

        #region "AccpacStandardError"
        public string GetAdvError()
        {
            int intWaningIndex = 0;
            string str_StdError="";
            try
            {
                //AccpacCurrentSession = pSession;
                string strErrorDescription;      //Variable to store AAS View Error Message.
                int Index;                       //Variable for AAS Session Error collection Index.
                string pMsg;                     //Variable to store each AAS Session error colletcion error message.
                tagErrorPriority pPriority;      //Variable to store AAS Error Message Priority.
                string pSource;                     //Variable to store AAS Error Message Time.
                string pErrCode;                   //Variable to store AAS Error Code.
                string pHelpFile;                   //Variable to store AAS Error Message Help File.
                int pHelpID;                    //Variable to store AAS Error Message Help ID.

                //Set the default value of the variable.
                strErrorDescription = "";
                Index = 0;
                pMsg = "";
                pSource = "";
                pPriority = tagErrorPriority.ERRPRI_SEVERROR;
                pErrCode = "";
                pHelpFile = "";
                pHelpID = 0;

                //Check Accpac Session Object is created.
                //If mDBLinkCmpRW.Session Is Nothing = True Then
                if (pSession == null)
                {
                    strErrorDescription = "Accpac Session is not Created.";
                    return strErrorDescription;
                }//if (AccpacCurrentSession == null)
                //Check Accpac session is Open.
                if (pSession.IsOpened == false)
                {
                    strErrorDescription = "Accpac Session is not Open.";
                    return strErrorDescription;
                }//if (AccpacCurrentSession.IsOpened == false)
                //Check Session Error collection contains any Errors.
                if (pSession.Errors.Count > 0)
                {
                    //Commented since it returns only one error message.
                    //strErrorDescription = mSession.Errors.Item(mSession.Errors.count - 1)
                    //Scan Session Error Collection.
                    for (Index = 0; Index <= pSession.Errors.Count - 1; Index++)
                    {
                        //Get Session Error Message.
                        pSession.Errors.Get(Index, out pMsg, out pPriority, out pSource, out pErrCode, out pHelpFile, out pHelpID);
                        //Check AAS Error Message Priority.
                        switch (pPriority)
                        {
                            case tagErrorPriority.ERRPRI_ERROR:
                                strErrorDescription = strErrorDescription + "Error: ";
                                break;
                            case tagErrorPriority.ERRPRI_MESSAGE:
                                strErrorDescription = strErrorDescription + "Information: ";
                                break;
                            case tagErrorPriority.ERRPRI_SECURITY:
                                strErrorDescription = strErrorDescription + "Security violation: ";
                                break;
                            case tagErrorPriority.ERRPRI_SEVERROR:
                                strErrorDescription = strErrorDescription + "Critical error: ";
                                break;
                            case tagErrorPriority.ERRPRI_WARNING:
                                //strErrorDescription = strErrorDescription + "\nWarning: ";
                                break;
                        }//switch (pPriority)
                         //Store Error Message
                        strErrorDescription = strErrorDescription + pMsg;

                    }//for(Index = 0; Index<= AccpacCurrentSession.Errors.Count - 1; Index++)
                    //Clear Session Error Collection.   
                    pSession.Errors.Clear();
                }//if(AccpacCurrentSession.Errors.Count > 0)
                intWaningIndex = 0;
                intWaningIndex = strErrorDescription.IndexOf("Warning. ");
                if (intWaningIndex >= 0)
                    str_StdError = strErrorDescription.Remove(intWaningIndex);
                else
                    str_StdError = strErrorDescription;
                return str_StdError;
            }//try
            catch (Exception ex)
            {
                if (pSession.Errors.Item(pSession.Errors.Count - 1).ToString() != null)
                {
                    intWaningIndex = 0;
                    intWaningIndex = pSession.Errors.Item(pSession.Errors.Count - 1).ToString().IndexOf("Warning. ");
                    str_StdError = pSession.Errors.Item(pSession.Errors.Count - 1).ToString().Remove(intWaningIndex);
                    pSession.Errors.Clear();
                    return str_StdError;
                }
                else
                    return ex.Message;
            }
            finally { GC.Collect(); }
        }
        #endregion "AccpacStandardError"

        #region "Accpac Date functions"

        //S:GT:KIRT:06262018: Function for Getting Sage 300 system format date
        public string GetSage300FormattedDate(string p_str_Data)
        {
            try
            {
                if (p_str_Data != "")
                {
                    //p_str_Data = DateTime.Parse(p_str_Data.Substring(4, 2) + "/" + p_str_Data.Substring(6, 2) + "/" + p_str_Data.Substring(0, 4)).ToShortDateString();
                    DateTime dd = new DateTime(int.Parse(p_str_Data.Substring(0, 4)), int.Parse(p_str_Data.Substring(4, 2)), int.Parse(p_str_Data.Substring(6, 2)));
                    p_str_Data = dd.ToShortDateString();
                }
                return p_str_Data;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally { GC.Collect(); }
        }
        //E:GT:KIRT:06262018: Function for Getting Sage 300 system format date

        //S:GT:KIRT:06262018: Function for Getting Sage 300 DB format date(YYYYMMDD) 
        public string GetSage300DBFormattedDate(string p_str_Data)
        {
            try
            {
                if (p_str_Data != "")
                {
                    DateTime dt;
                    dt = DateTime.Parse(p_str_Data);
                    p_str_Data = dt.Year.ToString("0000") + dt.Month.ToString("00") + dt.Day.ToString("00");
                }
                return p_str_Data;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally { GC.Collect(); }
        }
        //E:GT:KIRT:06262018: Function for Getting Sage 300 DB format date(YYYYMMDD) 
        #endregion "Accpac Date functions"

        #region "Setting Comments and Instructions"
        protected bool setCommentsInstructions(ref AccpacCOMAPI.AccpacView aVw_Comment, ref AccpacCOMAPI.AccpacViewFields aVw_CommentsFields, string CommentType, string CommentText)
        {
            try
            {
                aVw_Comment.RecordCreate(0);
                if (CommentType.ToUpper() == "INSTRUCTION" || CommentType == "2")
                    CommentType = "2";
                else
                    CommentType = "1";
                aVw_CommentsFields.Item("COMMENTTYP").PutWithoutVerification(CommentType);
                aVw_CommentsFields.Item("COMMENT").PutWithoutVerification(CommentText);   //Comment
                aVw_Comment.Insert();
                aVw_Comment.Read();
                return true;
            }
            catch
            {
                objLogFile.LogWrite(GetAdvError(), "FAIL");
                return false;
            }
            finally { GC.Collect(); }
        }//protected bool setOECommentsInstructions(ref AccpacCOMAPI.AccpacView aVw_Comments, ref AccpacCOMAPI.AccpacViewField aVw_CommentsFields, string CommentType, string CommentText)
        #endregion "Setting Comments and Instructions"
        

        #region Open close  OE views
        /// <summary>
        /// Open OE Invoice Views
        /// </summary>
        /// <returns></returns>
        public bool OpenOEInvoiceViews()
        {
            try
            {
                //ErrorLogs.WriteLogs("Opening OE Invoice Views.", ReadConfig.DebugLog_FileName, GetCurrentMethodName());

                mDBLinkCmpRW.OpenView("OE0520", out OEORD1header);
                OEORD1headerFields = OEORD1header.Fields;

                mDBLinkCmpRW.OpenView("OE0500", out OEORD1detail1);
                OEORD1detail1Fields = OEORD1detail1.Fields;

                mDBLinkCmpRW.OpenView("OE0740", out OEORD1detail2);
                OEORD1detail2Fields = OEORD1detail2.Fields;

                mDBLinkCmpRW.OpenView("OE0180", out OEORD1detail3);
                OEORD1detail3Fields = OEORD1detail3.Fields;

                mDBLinkCmpRW.OpenView("OE0526", out OEORD1detail4);
                OEORD1detail4Fields = OEORD1detail4.Fields;

                mDBLinkCmpRW.OpenView("OE0522", out OEORD1detail5);
                OEORD1detail5Fields = OEORD1detail5.Fields;

                mDBLinkCmpRW.OpenView("OE0508", out OEORD1detail6);
                OEORD1detail6Fields = OEORD1detail6.Fields;

                mDBLinkCmpRW.OpenView("OE0507", out OEORD1detail7);
                OEORD1detail7Fields = OEORD1detail7.Fields;

                mDBLinkCmpRW.OpenView("OE0501", out OEORD1detail8);
                OEORD1detail8Fields = OEORD1detail8.Fields;

                mDBLinkCmpRW.OpenView("OE0502", out OEORD1detail9);
                OEORD1detail9Fields = OEORD1detail9.Fields;

                mDBLinkCmpRW.OpenView("OE0504", out OEORD1detail10);
                OEORD1detail10Fields = OEORD1detail10.Fields;

                mDBLinkCmpRW.OpenView("OE0506", out OEORD1detail11);
                OEORD1detail11Fields = OEORD1detail11.Fields;

                mDBLinkCmpRW.OpenView("OE0503", out OEORD1detail12);
                OEORD1detail12Fields = OEORD1detail12.Fields;

                OEORD1header.Compose(new object[] { OEORD1detail1, null, OEORD1detail3, OEORD1detail2, OEORD1detail4, OEORD1detail5 });


                OEORD1detail1.Compose(new object[] { OEORD1header, OEORD1detail8, OEORD1detail12, OEORD1detail9, OEORD1detail6, OEORD1detail7 });


                OEORD1detail2.Compose(new object[] { OEORD1header });


                OEORD1detail3.Compose(new object[] { OEORD1header, OEORD1detail1 });


                OEORD1detail4.Compose(new object[] { OEORD1header });


                OEORD1detail5.Compose(new object[] { OEORD1header });


                OEORD1detail6.Compose(new object[] { OEORD1detail1 });


                OEORD1detail7.Compose(new object[] { OEORD1detail1 });


                OEORD1detail8.Compose(new object[] { OEORD1detail1 });


                OEORD1detail9.Compose(new object[] { OEORD1detail1, OEORD1detail10, OEORD1detail11 });


                OEORD1detail10.Compose(new object[] { OEORD1detail9 });


                OEORD1detail11.Compose(new object[] { OEORD1detail9 });


                OEORD1detail12.Compose(new object[] { OEORD1detail1 });

                //ErrorLogs.WriteLogs("Opened OE Invoice Views.", ReadConfig.DebugLog_FileName, GetCurrentMethodName());
                return true;
            }
            catch (Exception ex)
            {
                string strAdvError = GetAdvError().ToString();
                //ErrorLogs.WriteLogs(ex.Message.ToString() + Environment.NewLine + "Sage 300 Error: " + strAdvError, ReadConfig.ImportLogs);
                //Sage300CurrentSession.Errors.Clear();
                return false;
            }
            finally
            { GC.Collect(); }
        }


        /// <summary>
        /// Closed OE Invoice Views
        /// </summary>
        /// <returns></returns>
        public bool CloseOEInvoiceViews()
        {
            try
            {
                //ErrorLogs.WriteLogs("Closing OE Invoice Views.", ReadConfig.DebugLog_FileName, GetCurrentMethodName());

                OEORD1header.Close();
                OEORD1headerFields = null;

                OEORD1detail1.Close();
                OEORD1detail1Fields = null;

                //OEORD1detail2.Close();
                //OEORD1detail2Fields = null;

                //OEORD1detail3.Close();
                //OEORD1detail3Fields = null;

                //OEORD1detail4.Close();
                //OEORD1detail4Fields = null;

                //OEORD1detail5.Close();
                //OEORD1detail5Fields = null;

                //OEORD1detail6.Close();
                //OEORD1detail6Fields = null;

                //OEORD1detail7.Close();
                //OEORD1detail7Fields = null;

                //OEORD1detail8.Close();
                //OEORD1detail8Fields = null;

                //OEORD1detail9.Close();
                //OEORD1detail9Fields = null;

                //OEORD1detail10.Close();
                //OEORD1detail10Fields = null;

                //OEORD1detail11.Close();
                //OEORD1detail11Fields = null;

                //OEORD1detail12.Close();
                //OEORD1detail12Fields = null;

                //ErrorLogs.WriteLogs("Closed OE Invoice Views.", ReadConfig.DebugLog_FileName, GetCurrentMethodName());
                return true;
            }
            catch (Exception ex)
            {
                string strAdvError = GetAdvError().ToString();
                //ErrorLogs.WriteLogs(ex.Message.ToString() + Environment.NewLine + "Sage 300 Error: " + strAdvError, ReadConfig.ImportLog_FileName);
                //Sage300CurrentSession.Errors.Clear();
                return false;
            }
            finally
            { GC.Collect(); }
        }
        #endregion open close view

        #region Open Close CreditNote Views
        public bool OpenCreditNoteViews()
        {
            try
            {
                mDBLinkCmpRW.OpenView ("OE0240", out OECRD1header);
                OECRD1headerFields = OECRD1header.Fields;
                mDBLinkCmpRW.OpenView ("OE0220",out OECRD1detail1);
                OECRD1detail1Fields = OECRD1detail1.Fields;
                mDBLinkCmpRW.OpenView ("OE0140",out OECRD1detail2); 
                OECRD1detail2Fields = OECRD1detail2.Fields;
                mDBLinkCmpRW.OpenView ("OE0226",out OECRD1detail3);
                OECRD1detail3Fields = OECRD1detail3.Fields;
                mDBLinkCmpRW.OpenView("OE0227", out OECRD1detail4);
                OECRD1detail4Fields = OECRD1detail4.Fields;
                mDBLinkCmpRW.OpenView ("OE0242",out OECRD1detail5);
                OECRD1detail5Fields = OECRD1detail5.Fields;
                mDBLinkCmpRW.OpenView ("OE0221",out OECRD1detail6);
                OECRD1detail6Fields = OECRD1detail6.Fields;
                mDBLinkCmpRW.OpenView ("OE0222",out OECRD1detail7);
                OECRD1detail7Fields = OECRD1detail7.Fields;
                mDBLinkCmpRW.OpenView ("OE0224",out OECRD1detail8);
                OECRD1detail8Fields = OECRD1detail8.Fields;
                mDBLinkCmpRW.OpenView ("OE0225",out OECRD1detail9);
                OECRD1detail9Fields = OECRD1detail9.Fields;
                mDBLinkCmpRW.OpenView ("OE0223",out OECRD1detail10);
                OECRD1detail10Fields = OECRD1detail10.Fields;

                OECRD1header.Compose(new object[] { OECRD1detail1, null, OECRD1detail2, OECRD1detail5});

                OECRD1detail1.Compose(new object[] { OECRD1header, OECRD1detail6, OECRD1detail10, OECRD1detail7, OECRD1detail4, OECRD1detail3 });

                OECRD1detail2.Compose(new object[] { OECRD1header });

                OECRD1detail3.Compose(new object[] { OECRD1detail1 });

                OECRD1detail4.Compose(new object[] { OECRD1detail1 });

                OECRD1detail5.Compose(new object[] { OECRD1header });

                OECRD1detail6.Compose(new object[] { OECRD1detail1 });

                OECRD1detail7.Compose(new object[] { OECRD1detail1, OECRD1detail8, OECRD1detail9 });

                OECRD1detail8.Compose(new object[] { OECRD1detail7 });

                OECRD1detail9.Compose(new object[] { OECRD1detail7 });

                OECRD1detail10.Compose(new object[] { OECRD1detail1 });


                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Open Close CreditNote Views

        #region Moving File
        public bool MoveFile(string pstr_FileName, string pstr_SourceDirectory, string pstr_DestinationDirectory,string strLog)
        {
            try
            {
                string str_pstrFileName_New = "";
                string sourceFile = Path.Combine(pstr_SourceDirectory, pstr_FileName);
                string strFileExt = pstr_FileName.Substring(pstr_FileName.LastIndexOf('.'), pstr_FileName.Length-(pstr_FileName.LastIndexOf('.')));
                str_pstrFileName_New = pstr_FileName.Replace(strFileExt, "") + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss")+ strFileExt;
                string destFile = Path.Combine(pstr_DestinationDirectory, str_pstrFileName_New);

                if (!Directory.Exists(pstr_DestinationDirectory))
                {
                    Directory.CreateDirectory(pstr_DestinationDirectory);
                }//if (!Directory.Exists(pstr_DestinationDirectory))
                if (File.Exists(destFile))
                    File.Delete(destFile);
                File.Move(sourceFile, destFile);
                objLogFile.LogWrite("File : " + str_pstrFileName_New + " moved successfully to directory : " + pstr_DestinationDirectory, strLog);
                return true;
            }
            catch (Exception ex)
            {
                objLogFile.LogWrite(ex.Message, "FAIL");
                return false;
            }
            finally { GC.Collect(); }
        }
        #endregion Moving File

    }
}
