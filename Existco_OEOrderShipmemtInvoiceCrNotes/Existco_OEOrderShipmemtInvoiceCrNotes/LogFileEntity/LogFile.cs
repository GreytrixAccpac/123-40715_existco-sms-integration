﻿//Begin /GT/11-26-2014/Mayur

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Existco_OEOrderShipmemtInvoiceCrNotes.LogFileEntity
{
    public class LogFile
    {
        /// <summary>
        /// <Executable File Path></Executable File Path>
        /// </summary>
        /// 
        private string objmexePath_Error = "";
        private string objmexePath_Success = "";
        private string str_title = string.Empty;
        public long long_ErrorCnt;
        public string str_SuccessFileName = string.Empty;
        public string str_FailFileName = string.Empty;

        private string str_ErrorFilePrefix = "ExistcoSMSImportError";
        private string str_SuccessFilePrefix = "ExistcoImportSuccess";
        private string str_FileExtension = ".log";
        private string str_FileNameDateFormat = "yyyyMMdd_HHmmss";

        /// <summary>
        /// <LogFile Contstructor></LogFile Contstructor>
        /// </summary>
        public LogFile(string MsgTitle, string ErrorLogPath, string SuccessLogPath)
        {              
            objmexePath_Error = ErrorLogPath;
            objmexePath_Success = SuccessLogPath;
            str_title=MsgTitle;
            long_ErrorCnt = 0;
            CreateLogFile("SUCCESS");
            CreateLogFile("FAIL");
        }

        /// <summary>
        /// <Call Function To Write Log Details></Call Function To Write Log Details>
        /// </summary>
        public void LogWrite(string objDate, string objProcessType, string objSucessfail, string Comments)
        {
            if (objmexePath_Error == "")
            {
                objmexePath_Error = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            Directory.CreateDirectory(objmexePath_Error);
            if (objmexePath_Success == "")
            {
                objmexePath_Success = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            Directory.CreateDirectory(objmexePath_Success);
            try
            {
                if (objSucessfail == "SUCCESS")
                {
                    //str_SuccessFileName = str_SuccessFilePrefix + "_" + DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    using (StreamWriter w = File.AppendText(objmexePath_Success + str_SuccessFileName))
                    {
                        Log(objDate, objProcessType, objSucessfail, Comments, w);
                        long_ErrorCnt = long_ErrorCnt + 1;
                    }
                }
                else
                {
                    //str_FailFileName = str_ErrorFilePrefix +"_"+ DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    using (StreamWriter w = File.AppendText(objmexePath_Error + str_FailFileName))
                    {
                        Log(objDate, objProcessType, objSucessfail, Comments, w);
                        long_ErrorCnt = long_ErrorCnt + 1;
                    }
                }
                
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message,str_title);
            }
        }

        public void LogWrite(string Comments,string objSucessfail)
        {
            if (objmexePath_Error == "")
            {
                objmexePath_Error = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            //Directory.CreateDirectory(objmexePath_Error);
            if (objmexePath_Success == "")
            {
                objmexePath_Success = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            //Directory.CreateDirectory(objmexePath_Success);
            try
            {

                if (objSucessfail == "SUCCESS")
                {
                    //str_FileName = str_SuccessFilePrefix +"_"+ DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    using (StreamWriter w = File.AppendText(objmexePath_Success + str_SuccessFileName))
                    {
                        Log(Comments, w);
                        long_ErrorCnt = long_ErrorCnt + 1;
                    }
                }
                else
                {
                    //str_FileName = str_ErrorFilePrefix +"_"+ DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    using (StreamWriter w = File.AppendText( objmexePath_Error + str_FailFileName))
                    {
                        Log(Comments, w);
                        long_ErrorCnt = long_ErrorCnt + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, str_title);
            }
        }

        /// <summary>
        /// <Generate Log File ></Generate Log File >
        /// </summary>
        public void CreateLogFile( string objSucessfail)
        {
            try
            {
                if (objmexePath_Error == "")
                {
                    objmexePath_Error = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                }
                if (objmexePath_Success == "")
                {
                    objmexePath_Success = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                }

                if (objSucessfail == "SUCCESS")
                {
                    str_SuccessFileName = str_SuccessFilePrefix +"_"+ DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    Directory.CreateDirectory(objmexePath_Success);
                    using (StreamWriter w = File.AppendText(objmexePath_Success + "\\" + str_SuccessFileName))
                    {
                        w.WriteLine("\n\n***************************************************************************************************************************************************************************");
                        w.WriteLine("{0} {1} {2}", objSucessfail, DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("***************************************************************************************************************************************************************************");
                    }
                }
                else
                {
                    str_FailFileName = str_ErrorFilePrefix +"_"+ DateTime.Now.ToString(str_FileNameDateFormat) + str_FileExtension;
                    Directory.CreateDirectory(objmexePath_Error);
                    using (StreamWriter w = File.AppendText(objmexePath_Error + "\\" + str_FailFileName))
                    {
                        w.WriteLine("\n\n***************************************************************************************************************************************************************************");
                        w.WriteLine("{0} {1} {2}", objSucessfail, DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("***************************************************************************************************************************************************************************");
                    }
                }
             
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.Message, str_title);

            }
        }

        /// <summary>
        /// <Log Writing Function></Log Writing Function>
        /// </summary>
        public void Log(string objDate, string objProcessType , string objSucessfail,string Comments, TextWriter objtxtWriter)
        {
            try
            {
                objtxtWriter.WriteLine("" + objDate + " : " + objProcessType + " : " + objSucessfail + " : " + Comments + "");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, str_title);
            }
        }
        public void Log(string Comments, TextWriter objtxtWriter)
        {
            try
            {
                objtxtWriter.WriteLine( Comments );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, str_title);
            }
        }
        
    }
}
