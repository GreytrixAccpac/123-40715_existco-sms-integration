﻿namespace Existco_OEOrderShipmemtInvoiceCrNotes
{
    partial class frmImportState
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportState));
            this.lblImportStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblImportStatus
            // 
            this.lblImportStatus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblImportStatus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblImportStatus.Location = new System.Drawing.Point(13, 18);
            this.lblImportStatus.Name = "lblImportStatus";
            this.lblImportStatus.Size = new System.Drawing.Size(310, 20);
            this.lblImportStatus.TabIndex = 0;
            // 
            // frmImportState
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 52);
            this.Controls.Add(this.lblImportStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmImportState";
            this.Text = "Existco Import Status";
            this.Load += new System.EventHandler(this.frmImportState_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblImportStatus;
    }
}

