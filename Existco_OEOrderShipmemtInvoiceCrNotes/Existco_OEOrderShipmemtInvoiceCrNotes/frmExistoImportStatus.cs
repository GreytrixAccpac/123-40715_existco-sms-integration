﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Existco_OEOrderShipmemtInvoiceCrNotes.AccpacEntities;
using Existco_OEOrderShipmemtInvoiceCrNotes.LogFileEntity;
using System.Configuration;
using System.IO;
using AccpacCOMAPI;
using System.Globalization;

namespace Existco_OEOrderShipmemtInvoiceCrNotes
{
    public partial class frmImportState : Form
    {
        LogFile logFile;
        private clsAccpac AccpacObj;

        #region "Configue File parameters"
        public string config_Sage300CmpDBID = "";
        public string config_Sage300User = "";
        public string config_Sage300Password = "";
        public string config_AppID = "";
        string config_ProgramName = "";
        string config_Sage300Version = "";
        string config_ImportFileLocation = "";
        string config_ProcessedFilesLocation = "";
        string config_ArchivedDirectory = "";
        string config_ErrorDirectory = "";
        string config_ErrorLogFolder = "";
        string config_SuccessLogFolder = "";
        string filename = "";
        string strRunScheduler0Desktop1 = "0";
        string str_CUSTDISCITEM = "CUSTDISCOUNT";

        #endregion "Configue File parameters"

        #region "Constatns"
        const string APP_TITLE = "Existco SMS Import";
        const string ERROR_TITLE = "Error";
        const string INFO_TITLE = "Information";
        #endregion "Constatns"

        string Location = String.Empty;
        DataTable tblFileData = null;
        DataTable ValidatedTable = null;
        DataTable ValidatedCreditNotetbl = null;
        string PayTerms = string.Empty;


        public frmImportState(string strArg_CmpName, string strArg_User, string strArg_Pwd, string str_RunThrough0S1D)
        {
            //config_Sage300CmpDBID = ConfigurationManager.AppSettings["Sage300CompanyDatabaseID"];
            //config_Sage300User = ConfigurationManager.AppSettings["Sage300User"];
            //config_Sage300Password = ConfigurationManager.AppSettings["Sage300Password"];

            config_Sage300CmpDBID = strArg_CmpName;
            config_Sage300User = strArg_User;
            config_Sage300Password = strArg_Pwd;
            strRunScheduler0Desktop1 = str_RunThrough0S1D;

            InitializeComponent();
        }

        #region "Common Functions"
        public bool ReadConfigKeys()
        {
            try
            {
                ConfigurationManager.RefreshSection("appSettings");
                config_AppID = ConfigurationManager.AppSettings["AppID"];
                config_ProgramName = ConfigurationManager.AppSettings["ProgramName"];
                config_Sage300Version = ConfigurationManager.AppSettings["Sage300Version"];
                config_ImportFileLocation = ConfigurationManager.AppSettings["ImportFileLocation"];
                config_ErrorLogFolder = ConfigurationManager.AppSettings["ErrorLogFolder"];
                config_SuccessLogFolder = ConfigurationManager.AppSettings["SuccessLogFolder"];
                config_ProcessedFilesLocation = ConfigurationManager.AppSettings["ProcessedFilesLocation"];
                config_ArchivedDirectory = ConfigurationManager.AppSettings["ArchivedDirectory"];
                config_ErrorDirectory = ConfigurationManager.AppSettings["ErrorDirectory"];
                return true;
            }//try
            catch (Exception ex)
            {
                logFile.LogWrite("Error while Reading Config file parameters " + ex.Message, "FAIL");
                return false;

            }
            finally { GC.Collect(); }
        }

        //S:GT:KIRT:Finction to Validate file paths 
        public bool ValidateFilePath(string FilePath, string filetype)
        {
            try
            {
                if (System.IO.Directory.Exists(FilePath) == true)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { lblImportStatus.Text = "Error while validating Paths : " + ex.Message; return false; }
            finally { GC.Collect(); }
        }

        #endregion "Common Functions"

        
        /// <summary>
        /// GET ALL THE Import Files of OEInvoice from IEFILES Folder
        /// </summary>
        /// <returns></returns>
        private void GetImportOEINvoiceFileList(ref FileInfo[] files, ref string[] filesInfo)
        {
            try
            {
                string str_FilePrefix = "SMS";
                DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(config_ImportFileLocation);
                files = hdDirectoryInWhichToSearch.GetFiles(str_FilePrefix + "*.txt");
                int i = 0;
                foreach (FileInfo foundFile in files)
                {
                    filesInfo[i] = foundFile.FullName;
                    i = i + 1;
                }//foreach (FileInfo foundFile in files)
            }//try
            catch (Exception ex)
            {
                logFile.LogWrite("Error while getting files from import folder : " + ex.Message, ERROR_TITLE);
            }//catch (Exception ex)
            finally
            { GC.Collect(); }
        }

        /// <summary>
        /// Return the name of the method that called this one for error. 
        /// </summary>
        /// <returns></returns>

        private void frmImportState_Load(object sender, EventArgs e)
        {
            try
            {
                bool exceptionOccursInv = false;
                bool exceptionOccursCr = false;
                ReadConfigKeys();
                //Checking Log file Path Validity
                if (!Directory.Exists(config_ErrorLogFolder))
                {
                   // MessageBox.Show("Invalid path specified for ErrorLog location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for ErrorLog location. Please check config file");
                }
                else if (!Directory.Exists(config_SuccessLogFolder))
                {
                    //MessageBox.Show("Invalid path specified for SuccessLog location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for SuccessLog location. Please check config file");
                }
                else if (!Directory.Exists(config_ErrorLogFolder))
                {
                    //MessageBox.Show("Invalid path specified for Processed files location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for processed files location. Please check config file");
                }
                else if (!Directory.Exists(config_ImportFileLocation))
                {
                   // MessageBox.Show("Invalid path specified for Import files location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for Import file location. Please check config file");
                }
                else if (!Directory.Exists(config_ArchivedDirectory))
                {
                    //MessageBox.Show("Invalid path specified for Archived directory location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for Archive directory location. Please check config file");
                }
                else if (!Directory.Exists(config_ErrorDirectory))
                {
                    //MessageBox.Show("Invalid path specified for Archived directory location. Please check config file", APP_TITLE); this.Close();
                    System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Invalid path specified for Error directory location. Please check config file");
                }
                
                else // All Paths are valid
                {
                    #region "LogFile Initialization"
                    //Creating Log files
                    logFile = new LogFile(APP_TITLE, config_ErrorLogFolder, config_SuccessLogFolder);
                    #endregion "LogFile Initialization"

                    #region "SMS Files Reading"
                    string[] csvFiles = Directory.GetFiles(config_ImportFileLocation, "*.txt");
                    //reading each CSVFIleData
                    DataTable dtFileData = new DataTable();
                    //AddColsToDataTable(ref dtFileData);
                    //foreach (string Curfile in csvFiles)
                    //{
                    //   ReadtxtFile(Curfile);
                    //}

                    #endregion "SMS Files Reading"

                    #region "AccpacSession Initialization"
                    //Creating Accpac class object
                    AccpacObj = new clsAccpac(config_AppID, config_ProgramName, config_Sage300Version, config_Sage300User, config_Sage300Password, config_Sage300CmpDBID, ref logFile);

                    if (AccpacObj.CreateAccpacSession())
                    {
                        this.Text = AccpacObj.pSession.CompanyID + "-" + APP_TITLE;
                        lblImportStatus.Text = "Accpac Session created.";
                        lblImportStatus.Refresh();
                        long cntFileCnt = 0;
                        long cntFilesImported = 0;
                        foreach (string Curfile in csvFiles)
                        {
                            cntFileCnt = cntFileCnt + 1;
                            ReadtxtFile(Curfile);
                            ValidatedTable = new DataTable();
                            ValidatedTable = tblFileData.Clone();
                            ValidatedCreditNotetbl = new DataTable();
                            ValidatedCreditNotetbl = tblFileData.Clone();
                            
                                //ReadInvoiceCrRecordsSeparately(tblFileData.Rows[counter]);
                                //S:GT:Change in read logic based on document total
                                SeparateCRandINV(ref tblFileData);



                            AddOEInvoice(ValidatedTable, out exceptionOccursInv);
                            AddCreditNote(ValidatedCreditNotetbl, out exceptionOccursCr);

                            if (exceptionOccursInv == false && exceptionOccursCr == false)
                            {
                                AccpacObj.MoveFile(filename, config_ImportFileLocation, config_ArchivedDirectory,"SUCCESS");
                                cntFilesImported = cntFilesImported + 1;
                            }// if (exceptionOccurs == false)
                            else
                            {
                                logFile.LogWrite("File : " + filename + " : Error while importing data from file.", ERROR_TITLE);
                                AccpacObj.MoveFile(filename, config_ImportFileLocation, config_ErrorDirectory, "FAIL");
                            }//else if (exceptionOccurs == false)
                        }//For
                        if (cntFileCnt == cntFilesImported)
                            lblImportStatus.Text = "All the files got imported successfully.";
                        else
                            lblImportStatus.Text = "All the files not got imported. Please look at the errorlog generated.";
                    }//if (AccpacObj.CreateAccpacSession())
                    else
                    {
                        System.Diagnostics.EventLog.WriteEntry("Existco SMS Import", "Unable to create Sage 300 session.");
                    }//else if (AccpacObj.CreateAccpacSession())
                    #endregion "AccpacSession Initialization"
                } // All paths valid
                if(strRunScheduler0Desktop1=="0")
                    this.Close();
            }
            catch (Exception ex)
            { lblImportStatus.Text = ex.Message; }
            finally
            {
                GC.Collect();
            }//finally
        }

        /// <summary>
        /// Read .txt files
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public DataTable ReadtxtFile(string FilePath)
        {
            filename = Path.GetFileName(FilePath);
            logFile.LogWrite("Reading file from import folder : " + FilePath , ERROR_TITLE);
            try
            {
                Location = filename.Substring(filename.LastIndexOf('.') - 2, 2);

            }
            catch 
            {
                logFile.LogWrite("Unable to get the Loation code form file name." + FilePath, ERROR_TITLE);
            }
            //Location = FileName[6].ToString().Remove(3);
            try
            {
                tblFileData = new DataTable();

                var fileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    string[] items;
                    var cols = streamReader.ReadLine().Split(',');
                    int colscount = cols.Count();
                    for (int col = 0; col < colscount; col++)
                        tblFileData.Columns.Add(new DataColumn(cols[col].ToString().Replace("\"","")));

                    DataRow dr = tblFileData.NewRow();

                    while (!streamReader.EndOfStream)
                    {
                        items = streamReader.ReadLine().Replace("\"","").Trim().Split(',');
                        tblFileData.Rows.Add(items);
                    }

                }
                return tblFileData;
            }
            catch (Exception ex)
            {
                logFile.LogWrite("Error while reading file from import folder : " + ex.Message, ERROR_TITLE);
                return null;
            }
            finally
            { GC.Collect(); }
        }

        /// <summary>
        /// Import the Order, Shipment and invoice
        /// </summary>
        /// <param name="objOEHeader"></param>
        private void AddOEInvoice(DataTable objDatatable,out bool excepOccursInv)
        {
            string oriOrdNum = string.Empty;
            bool HErr = false;
            bool DErr = false;
            excepOccursInv = false;
            int finalcountDe = 0;
            int insertcount = 0;
            int RowCount = 0;
            try
            { 
                if (AccpacObj.OpenOEInvoiceViews())
                {                    
                    for (int item = 0; item < objDatatable.Rows.Count; item++)
                    {
                        HErr = false;
                        DErr = false;
                        RowCount++;
                        try
                        {
                            if (oriOrdNum == "")
                            {
                                AccpacObj.OEORD1header.Cancel();
                                AccpacObj.OEORD1header.Init();
                                oriOrdNum = objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim();
                                if (!LocationwiseInvoice(oriOrdNum, Location))
                                {
                                    logFile.LogWrite("Error while importing invoice . Invoice Number : " + objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim() + " location wise '" + Location + "' perfix format does not match.", "FAIL");
                                    HErr = true; 
                                }

                                if (!ValidateInvoiceNumber(objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim(), "ORDER"))
                                {
                                    AccpacObj.OEORD1headerFields.Item("ORDNUMBER").set_Value(objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim());                                    // ' Order Number
                                }//if(ValidateInvoiceNumber(objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim(), "Order",""))
                                else
                                {
                                    logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Order number already exist.", "FAIL");
                                    HErr = true;
                                }//Duplicate Order

                                if (ValidateCustomer(objDatatable.Rows[item][58].ToString().Replace("\"", "").Trim(), oriOrdNum))
                                {
                                    if (HErr == false)
                                    {
                                        AccpacObj.OEORD1headerFields.Item("CUSTOMER").set_Value(objDatatable.Rows[item][58].ToString().Replace("\"", "").Trim());              //' Customer Number
                                        AccpacObj.OEORD1headerFields.Item("PROCESSCMD").PutWithoutVerification("1");                      // ' Process OIP Command
                                        AccpacObj.OEORD1header.Process();
                                    }
                                }
                                else
                                {
                                    logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Customer Number : " + objDatatable.Rows[item][58].ToString().Replace("\"", "").Trim(), "FAIL");
                                    HErr = true;
                                }

                                try
                                {
                                    DateTime dt = DateTime.Parse((objDatatable.Rows[item][8].ToString().Trim().Replace("\"", "")).ToString());
                                    if(HErr==false)
                                        AccpacObj.OEORD1headerFields.Item("ORDDATE").set_Value(dt);                    //' Order Date
                                }//try
                                catch
                                {
                                    logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Invoice Date : " + objDatatable.Rows[item][8].ToString().Trim().Replace("\"", "").ToString(), "FAIL");
                                    HErr = true;
                                }//catch

                                if (!string.IsNullOrEmpty(ValidateLocation(Location, oriOrdNum)))
                                {
                                    if (HErr == false)
                                        AccpacObj.OEORD1headerFields.Item("LOCATION").set_Value(ValidateLocation(Location, oriOrdNum));         // ' Default Location Code
                                }//if (!string.IsNullOrEmpty(ValidateLocation(Location, oriOrdNum)))
                                else
                                {
                                    logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Header Location : " + Location + ".", "FAIL");
                                    HErr = true;
                                }
                                string validPayTerms = ValiadtePaymentTerms(objDatatable.Rows[item][40].ToString().Replace("\"", "").Trim(), oriOrdNum);
                                if (!string.IsNullOrEmpty(validPayTerms))
                                {
                                    if (HErr == false)
                                    {
                                        AccpacObj.OEORD1headerFields.Item("TERMS").set_Value(validPayTerms);                                   //' Terms Code
                                        AccpacObj.OEORD1header.Process();
                                        AccpacObj.OEORD1detail1.RecordClear();
                                    }//if (HErr == false)
                                    DErr = false;

                                }
                                else
                                {
                                    logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Terms code : " + validPayTerms + ".", "FAIL");
                                    HErr = true;
                                }

                                for (int detail = item; detail < objDatatable.Rows.Count; detail++)
                                {
                                    if (oriOrdNum == objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())
                                    {

                                        try
                                        {
                                            if (!string.IsNullOrEmpty(ValidateItem(objDatatable.Rows[detail][14].ToString(), objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())))
                                            {
                                                if (HErr == false)
                                                {
                                                    AccpacObj.OEORD1detail1.RecordCreate(tagViewRecordCreateEnum.VIEW_RECORD_CREATE_NOINSERT);
                                                    AccpacObj.OEORD1detail1Fields.Item("ITEM").set_Value(ValidateItem(objDatatable.Rows[detail][14].ToString(), objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim()));              //' Item
                                                    AccpacObj.OEORD1detail1Fields.Item("PROCESSCMD").PutWithoutVerification("1");      //' Process Command
                                                    AccpacObj.OEORD1detail1.Process();
                                                    AccpacObj.OEORD1detail1Fields.Item("PROCESSCMD").PutWithoutVerification("1");      //' Process Command
                                                    string loc = ValidateLocation(Location, objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim());
                                                    if (!string.IsNullOrEmpty(loc))
                                                        AccpacObj.OEORD1detail1Fields.Item("LOCATION").set_Value(loc);                      //' Location
                                                    else
                                                    {
                                                        logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Detail Location : " + Location + ".", "FAIL");
                                                        DErr = true;
                                                    }//if (!string.IsNullOrEmpty(loc))
                                                }//if (HErr == false)
                                            }//  if (!string.IsNullOrEmpty(ValidateItem(objDatatable.Rows[detail][14].ToString(), objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())))
                                            else
                                            {
                                                logFile.LogWrite("Error while importing invoice " + oriOrdNum + ": Invalid Item Number : " + objDatatable.Rows[detail][14].ToString() + ".", "FAIL");
                                                DErr = true;
                                            }//else if (!string.IsNullOrEmpty(ValidateItem(objDatatable.Rows[detail][14].ToString(), objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())))

                                            if (HErr == false && DErr == false)
                                            {
                                                try
                                                {
                                                    //S:GT:if item is CUSTDISCOUNT set Qty to -1
                                                    if (AccpacObj.OEORD1detail1Fields.Item("ITEM").get_Value() == str_CUSTDISCITEM)
                                                        AccpacObj.OEORD1detail1Fields.Item("QTYORDERED").set_Value(-1);                   //' Quantity Ordered
                                                    else
                                                        AccpacObj.OEORD1detail1Fields.Item("QTYORDERED").set_Value(objDatatable.Rows[detail][13].ToString().Replace("\"", "").Trim());                   //' Quantity Ordered
                                                    AccpacObj.OEORD1detail1Fields.Item("PRIUNTPRC").set_Value(objDatatable.Rows[detail][15].ToString().Replace("\"", "").Trim());                  // ' Pricing Unit Price
                                                    AccpacObj.OEORD1detail1Fields.Item("COSUNTCST").set_Value(objDatatable.Rows[detail][30].ToString().Replace("\"", "").Trim());                 // ' Costing Unit Cost
                                                    AccpacObj.OEORD1detail1.Insert();
                                                }
                                                catch (Exception ex)
                                                {
                                                    logFile.LogWrite("Error while importing Invoice " + oriOrdNum + ": Detail :" + objDatatable.Rows[detail][14].ToString() + " : " + AccpacObj.GetAdvError(), "FAIL");
                                                    DErr = true;
                                                }//catch (Exception ex)


                                            }//if (DErr == false)
                                            else
                                                HErr = true;
                                        }//try
                                        catch (Exception ex)
                                        {
                                            logFile.LogWrite("Error while importing Invoice " + oriOrdNum + ": Detail :" + objDatatable.Rows[detail][14].ToString() + " : " + AccpacObj.GetAdvError(), "FAIL");
                                            DErr = true;
                                        }//catch

                                        insertcount++;

                                        if (insertcount > 1)
                                        {
                                            finalcountDe++;
                                        }//if (insertcount > 1)
                                    }//if (oriOrdNum == objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())
                                    else { break; }
                                }//for (int item = 0; item < objDatatable.Rows.Count; item++)
                            }//if (oriOrdNum == "")
                            if (HErr == false && DErr == false)
                            {
                                AccpacObj.OEORD1detail1Fields.Item("LINENUM").PutWithoutVerification("-1");           // ' Line Number
                                AccpacObj.OEORD1detail1.Read();

                                AccpacObj.OEORD1headerFields.Item("GOSHIPALL").set_Value("1");                        //' Perform Ship All
                                AccpacObj.OEORD1header.Process();

                                AccpacObj.OEORD1headerFields.Item("INVPRODUCE").set_Value("1");                       // ' Invoice Will Be Produced

                                AccpacObj.OEORD1detail1Fields.Item("LINENUM").PutWithoutVerification("-1");           // ' Line Number
                                AccpacObj.OEORD1detail1.Read();
                                AccpacObj.OEORD1headerFields.Item("SHINUMBER").set_Value(objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim());                    //' Shipment Number
                                AccpacObj.OEORD1headerFields.Item("SHIDATE").set_Value(DateTime.Parse((objDatatable.Rows[item][8].ToString().Trim().Replace("\"", "")).ToString()));
                                AccpacObj.OEORD1headerFields.Item("INVNUMBER").set_Value(objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim());                    //' Invoice Number
                                AccpacObj.OEORD1headerFields.Item("INVDATE").set_Value(DateTime.Parse((objDatatable.Rows[item][8].ToString().Trim().Replace("\"", "")).ToString()));
                                AccpacObj.OEORD1headerFields.Item("OECOMMAND").set_Value("0");                       //' Process O/E Command
                                try
                                {
                                    AccpacObj.OEORD1header.Insert();
                                    logFile.LogWrite("Invoice Imported Sucessfully " + objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim(), "SUCCESS");
                                    AccpacObj.pSession.Errors.Clear();
                                }
                                catch (Exception ex)
                                {
                                    excepOccursInv = true;
                                    logFile.LogWrite("Error while importing Invoice " + oriOrdNum + ": " + AccpacObj.GetAdvError() + ex.Message, "FAIL");
                                    AccpacObj.pSession.Errors.Clear();
                                    HErr = false; DErr = false;
                                }
                            }//if (HErr == false && DErr == false)
                            else
                            {
                                logFile.LogWrite("Error while importing Invoice " + objDatatable.Rows[item][7].ToString().Replace("\"", "").Trim(), "FAIL");
                                excepOccursInv = true;
                            }//else if (HErr == false && DErr == false)
                            oriOrdNum = "";
                        item = finalcountDe;
                        }//try  
                        catch (Exception ex)
                        {
                            excepOccursInv = true;
                            logFile.LogWrite("Error while importing Invoice "+ oriOrdNum + ": " + AccpacObj.GetAdvError() + ex.Message, "FAIL");
                            HErr = true;DErr = true;
                            oriOrdNum = "";
                            if (RowCount > 1)
                            {
                                 finalcountDe++;
                                 item = finalcountDe;
                            }//if (RowCount > 1)
                            else
                            {
                                item = finalcountDe;
                            }//else if (RowCount > 1)

                        }// catch (Exception ex)
                    }
                }
             }
            catch (Exception ex)
            {
                logFile.LogWrite("Error while importing ." + AccpacObj.GetAdvError() + ex.Message, "FAIL");
                AccpacObj.pSession.Errors.Put("", tagErrorPriority.ERRPRI_ERROR, AccpacObj.GetAdvError(), "", "");
            }

            finally
            { GC.Collect(); }
        }

        /// <summary>
        /// Import credit note
        /// </summary>
        /// <param name="objdt"></param>
        private void AddCreditNote(DataTable objdt, out bool excepOccursCr)
        {
            bool DtlError = false;
            string oriInvNum = string.Empty;
            int finalCreNcountDe = 0;
            int insertLinecount = 0;
            int TotalRowCount = 0;
            excepOccursCr = false;
            try
            {
                string strCrNo = "";
                if (AccpacObj.OpenCreditNoteViews())
                {
                    for (int Credititem = 0; Credititem < objdt.Rows.Count; Credititem++)
                    { 
                        if (oriInvNum != objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() && oriInvNum != "")
                            oriInvNum = "";
                        try
                        {
                            if (oriInvNum == "")
                            {
                                oriInvNum = objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim();
                                strCrNo = objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim();
                                
                                if (!ValidateInvoiceNumber(objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim(), "CREDIT"))
                                {
                                    AccpacObj.OECRD1header.Cancel();
                                    AccpacObj.OECRD1header.Init();//' Credit/Debit Note Number
                                    AccpacObj.OECRD1headerFields.Item("CRDNUMBER").set_Value(objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim());
                                    if (ValidateCustomer(objdt.Rows[Credititem][58].ToString().Replace("\"", "").Trim(), strCrNo))
                                    {
                                        AccpacObj.OECRD1headerFields.Item("CUSTOMER").set_Value(objdt.Rows[Credititem][58].ToString().Replace("\"", "").Trim());                   //' Customer Number
                                        AccpacObj.OECRD1headerFields.Item("PROCESSCMD").PutWithoutVerification("1");         //' Process Command
                                        AccpacObj.OECRD1header.Process();
                                        //if (ValidateInvoiceNumber(objdt.Rows[Credititem][21].ToString().Replace("\"", "").Trim(), "INVOICE"))
                                        //{
                                        AccpacObj.OECRD1headerFields.Item("INVNUMBER").set_Value(objdt.Rows[Credititem][21].ToString().Replace("\"", "").Trim());                    //' Invoice Number

                                        //AccpacObj.OECRD1headerFields.Item("CRDDATE").set_Value(Convert.ToDateTime(GetSage300FormattedDate(objdt.Rows[Credititem][8].ToString().Trim())));        //' Credit/Debit Note Date                                                                                     //AccpacObj.OECRD1headerFields.Item("CRDDATE").set_Value("2021, 8, 24");        // ' Credit/Debit Note Dat
                                        try
                                        {
                                            DateTime dt = DateTime.Parse((objdt.Rows[Credititem][8].ToString().Trim().Replace("\"", "")).ToString());
                                            AccpacObj.OECRD1headerFields.Item("CRDDATE").set_Value(dt);                    //' Order Date
                                        }//try
                                        catch
                                        {
                                            logFile.LogWrite("Error while importing Credit Note " + strCrNo + ": Invalid Credit Note Date : " + objdt.Rows[Credititem][8].ToString().Trim().Replace("\"", "").ToString(), "FAIL");
                                            excepOccursCr = true;
                                        }//catch

                                        AccpacObj.OECRD1headerFields.Item("LOCATION").set_Value(ValidateLocation(Location, objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim()));                            //' Default Location Code
                                        AccpacObj.OECRD1detail1Fields.Item("LINENUM").PutWithoutVerification("-1");         //' Line Number
                                        AccpacObj.OECRD1detail1.Read();
                                        //S:GT:KIRT:01182023:Changes to create credit note as per detail lines and not based on invoice
                                        AccpacObj.OECRD1detail1.GoTop();
                                        while (AccpacObj.OECRD1detail1.Fetch())
                                        {
                                            AccpacObj.OECRD1detail1.Delete();
                                            AccpacObj.OECRD1detail1.GoNext();
                                        }//if (AccpacObj.OECRD1detail1.Fetch())
                                        AccpacObj.OECRD1detail1.Read();
                                        AccpacObj.OECRD1detail1.RecordClear();
                                        int crLineItemCnt = 0;
                                        DtlError = false;
                                        insertLinecount = 0;
                                        for (int i = Credititem; i < objdt.Rows.Count; i++)
                                        {
                                            
                                            crLineItemCnt = crLineItemCnt + 1;
                                            if (oriInvNum == objdt.Rows[i][7].ToString().Replace("\"", "").Trim())
                                            {
                                                TotalRowCount++;
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(ValidateItem(objdt.Rows[i][14].ToString(), objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim())))
                                                    {
                                                        AccpacObj.OECRD1detail1.RecordCreate(0);
                                                        AccpacObj.OECRD1detail1Fields.Item("ITEM").set_Value(ValidateItem(objdt.Rows[i][14].ToString(), objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim()));
                                                        AccpacObj.OECRD1detail1Fields.Item("PROCESSCMD").PutWithoutVerification("1");        // Process Command
                                                        AccpacObj.OECRD1detail1.Process();
                                                        AccpacObj.OECRD1detail1Fields.Item("LOCATION").set_Value(ValidateLocation(Location, objdt.Rows[i][Credititem].ToString().Replace("\"", "").Trim()));                           //' Location
                                                        //S:20230321:Changes as per discussion with Cliff to consider +ve value Qty when value is -ve 
                                                        if (double.Parse(objdt.Rows[i][13].ToString().Replace("\"", "").Trim()) >= 0)
                                                            AccpacObj.OECRD1detail1Fields.Item("QTYRETURN").set_Value(objdt.Rows[i][13].ToString().Replace("\"", "").Trim());                     //' Quantity Returned
                                                        else
                                                            AccpacObj.OECRD1detail1Fields.Item("QTYRETURN").set_Value(-1 * double.Parse(objdt.Rows[i][13].ToString().Replace("\"", "").Trim()));                  // ' Pricing Unit Price

                                               
                                                        //S:20230321:Changes as per discussion with Cliff to consider +ve value unit price when value is -ve 
                                                        if (double.Parse(objdt.Rows[i][15].ToString().Replace("\"", "").Trim())>=0)
                                                            AccpacObj.OECRD1detail1Fields.Item("PRIUNTPRC").set_Value(objdt.Rows[i][15].ToString().Replace("\"", "").Trim());                  // ' Pricing Unit Price
                                                        else
                                                            AccpacObj.OECRD1detail1Fields.Item("PRIUNTPRC").set_Value(-1*double.Parse(objdt.Rows[i][15].ToString().Replace("\"", "").Trim()));                  // ' Pricing Unit Price

                                                        //S:20230321:Changes as per discussion with Cliff to consider unit price when value is -ve 
                                                        AccpacObj.OECRD1detail1Fields.Item("COSUNTCST").set_Value(objdt.Rows[i][30].ToString().Replace("\"", "").Trim());                  //' Costing Unit Cost

                                                        if (AccpacObj.OECRD1detail1.Exists)
                                                            AccpacObj.OECRD1detail1.Update();
                                                        else
                                                            AccpacObj.OECRD1detail1.Insert();
                                                        AccpacObj.OECRD1detail1Fields.Item("LINENUM").PutWithoutVerification(-1 * crLineItemCnt + "");          //' Line Number
                                                        AccpacObj.OECRD1detail1.Read();
                                                        insertLinecount++;

                                                    }
                                                    else
                                                    {
                                                        logFile.LogWrite("Error while importing Credit Note " + oriInvNum + ": Invalid Item Number : " + objdt.Rows[Credititem][14].ToString() + ".", "FAIL");
                                                        DtlError = true;
                                                        insertLinecount++;
                                                    }//else if (!string.IsNullOrEmpty(ValidateItem(objDatatable.Rows[detail][14].ToString(), objDatatable.Rows[detail][7].ToString().Replace("\"", "").Trim())))
                                                }
                                                catch
                                                {
                                                    DtlError = true;
                                                    logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Invoice Number : " + objdt.Rows[Credititem][21].ToString().Replace("\"", "").Trim() + ": " + AccpacObj.GetAdvError(), "FAIL");
                                                    insertLinecount++;
                                                }
                                            }//if (oriInvNum == objdt.Rows[i][7].ToString().Replace("\"", "").Trim())
                                            else
                                            {break;}
                                            //if(insertLinecount>1)
                                            //    finalCreNcountDe++;
                                            
                                        }//for (int i = Credititem; i < objdt.Rows.Count; i++)
                                        if (Credititem<=objdt.Rows.Count && DtlError == false)
                                        {
                                            AccpacObj.OECRD1header.Process();
                                            try
                                            {
                                                AccpacObj.OECRD1header.Insert();
                                                logFile.LogWrite("Credit Note Imported Sucessfully " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim(), "SUCCESS");
                                            }//try
                                            catch (Exception ex)
                                            {
                                                logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Invoice Number : " + objdt.Rows[Credititem][21].ToString().Replace("\"", "").Trim() + ": " + AccpacObj.GetAdvError(), "FAIL");
                                                excepOccursCr = true;
                                            }
                                            
                                            oriInvNum = "";
                                            Credititem = Credititem + (insertLinecount-1);
                                        }//As No Error in credit note header and details
                                        else
                                        {
                                            logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Detail line error.", "FAIL");
                                            excepOccursCr = true;
                                        }//As there is Error in credit note details
                                    }//Customer valid
                                    else
                                    {
                                        logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Customer Number : " + objdt.Rows[Credititem][58].ToString().Replace("\"", "").Trim() + " does not exist or it is inactive", "FAIL");
                                        excepOccursCr = true;
                                    }//Invalid Customer
                                }//if (ValidateInvoiceNumber(objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim(), "Credit Note", ""))
                                else
                                {
                                    logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Credit Note Record already exist.", "FAIL");
                                    excepOccursCr = true;
                                }//Duplicate Crdit Note Number
                            }// Orginv==""
                            
                        }
                        catch (Exception ex)
                        {
                            excepOccursCr = true;
                            logFile.LogWrite("Error while importing Credit Note " + objdt.Rows[Credititem][7].ToString().Replace("\"", "").Trim() + ". Invoice Number : " + objdt.Rows[Credititem][21].ToString().Replace("\"", "").Trim() + ": " + AccpacObj.GetAdvError(), "FAIL");
                            oriInvNum = "";
                            //if (TotalRowCount > 1)
                            //{
                            //    finalCreNcountDe++;
                            //    Credititem = finalCreNcountDe;
                            //}
                            //else
                            //{
                            //    Credititem = finalCreNcountDe;
                            //}
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                excepOccursCr = true;
                logFile.LogWrite("Error while importing Credit Note. " + AccpacObj.GetAdvError() + ex.Message, "FAIL");
                AccpacObj.pSession.Errors.Put("", tagErrorPriority.ERRPRI_ERROR, AccpacObj.GetAdvError(), "", "");
            }
        }

        #region validations
        /// <summary>
        /// Check all validations 
        /// </summary>
        /// <param name="row_dt"></param>
        public void Validation(DataRow row_dt)
        {
            int validationcount = 0;
            string ORDNumber = row_dt[7].ToString();
            string str_InvNo;
            try
            {
                if (row_dt[58].ToString() != "" && row_dt[21].ToString().Replace("\"", "").Trim() == "")
                {
                    str_InvNo = "";
                    str_InvNo = row_dt[7].ToString().Replace("\"", "").Trim();

                    if (ValidateCustomer(row_dt[58].ToString(), str_InvNo))
                    {
                        validationcount++;
                    }
                    else
                    { logFile.LogWrite(row_dt[7].ToString() + " : Invalid Customer Number " + row_dt[58].ToString(), "FAIL"); }

                    if (ValidateItem(row_dt[14].ToString(), str_InvNo) != null)
                    {
                        validationcount++;
                    }
                    else
                    {
                        if (row_dt[14].ToString().Replace("\"", "").Trim() == "")
                            logFile.LogWrite(row_dt[7].ToString() + " : Item Number field is blank", "FAIL");
                        else
                            logFile.LogWrite(row_dt[7].ToString() + " : Invalid Item Number OR Item is Inactive " + row_dt[14].ToString(), "FAIL");
                    }

                    if (ValidateLocation(Location, str_InvNo) != null)
                    {
                        validationcount++;
                    }
                    else
                    { logFile.LogWrite(row_dt[7].ToString() + " : Invalid Location " + Location, "FAIL"); }

                    if (ValiadtePaymentTerms(row_dt[40].ToString(), str_InvNo) != null)
                    {
                        validationcount++;
                    }
                    else
                    { logFile.LogWrite("Invoice Number : " + str_InvNo + "***" + row_dt[7].ToString() + " : Invalid PaymentTerm " + PayTerms, "FAIL"); }

                    if (validationcount == 4)
                    {
                        DataRow drscuccess = ValidatedTable.NewRow();
                        drscuccess = row_dt;
                        ValidatedTable.ImportRow(drscuccess);   //only valid rows are copy in ValidatedTable Datatable
                        validationcount = 0;
                    }
                }

                //credit Note rows are validate
                if (row_dt[21].ToString().Replace("\"", "").Trim() != "")
                {
                    str_InvNo = "";
                    str_InvNo = row_dt[7].ToString().Replace("\"", "").Trim();
                    if (ValidateInvoiceNumber(row_dt[21].ToString(), "CREDIT"))
                    {
                        validationcount++;

                        if (ValidateCustomer(row_dt[58].ToString(), str_InvNo))
                        {
                            validationcount++;
                        }
                        else
                        { logFile.LogWrite("Invoice Number : " + str_InvNo + " Invalid Customer Number " + row_dt[58].ToString(), "FAIL"); }


                        if (ValidateItem(row_dt[14].ToString(), str_InvNo) != null)
                        {
                            validationcount++;
                        }
                        else
                        {
                            if (row_dt[14].ToString().Replace("\"", "").Trim() == "")
                                logFile.LogWrite("Invoice Number : " + str_InvNo + " Item Number field is blank", "FAIL");
                            else
                                logFile.LogWrite("Invoice Number : " + str_InvNo + " Invalid Item Number OR Item is Inactive " + row_dt[14].ToString(), "FAIL");
                        }

                        if (ValidateLocation(Location, str_InvNo) != null)
                        {
                            validationcount++;
                        }
                        else
                        { logFile.LogWrite("Invoice Number : " + str_InvNo + " Invalid Location " + Location, "FAIL"); }

                        if (ValiadtePaymentTerms(row_dt[40].ToString(), str_InvNo) != null)
                        {
                            validationcount++;
                        }
                        else
                        { logFile.LogWrite("Invoice Number : " + str_InvNo + " Invalid PaymentTerm. " + PayTerms, "FAIL"); }

                        if (validationcount == 5)
                        {
                            DataRow drscuccess1 = ValidatedCreditNotetbl.NewRow();
                            drscuccess1 = row_dt;
                            ValidatedCreditNotetbl.ImportRow(drscuccess1); //only valid credit note copy
                        }
                    }
                    else
                    {
                        logFile.LogWrite("Invalid Invoice Number : " + str_InvNo + " for create Credit Note " + row_dt[7].ToString(), "FAIL");

                    }
                }
            }

            catch (Exception ex)
            {
                logFile.LogWrite("Error while validating import fiels : " + ex.Message, ERROR_TITLE);
            }
        }

/// <summary>
/// Function to separate Credit note and invoice record based on total column
/// </summary>
/// <param name="sItem"></param>
/// <returns></returns>
/// 
        public void SeparateCRandINV(ref DataTable dt)
        {
            try
            {
                string str_CurLineInv = "";
                double dbl_InvTot = 0;
                int lng_dtlCnt = 0;

                dt.Columns.Add("DblTotal",typeof(double));
                dt.AcceptChanges();
                for (int counter = 0; counter < dt.Rows.Count; counter++)
                {
                    dt.Rows[counter][dt.Columns.Count - 1] = double.Parse(dt.Rows[counter][18].ToString().Replace("\"", ""));
                    dt.AcceptChanges();
                }

                for (int counter = 0; counter < dt.Rows.Count; counter++)
                {
                    str_CurLineInv = dt.Rows[counter][7].ToString().Replace("\"", "").Trim();
                    // DataRow[] InvDRS;
                    
                    string strInv = "[" + dt.Columns[7].ColumnName +"] = '"+ str_CurLineInv + "'"; 
                    DataRow[] InvDRS = dt.Select(strInv);

                    if (InvDRS.Length == 1 && double.Parse(dt.Rows[counter][13].ToString().Replace("\"", "")) == 0 && double.Parse(dt.Rows[counter][15].ToString().Replace("\"", "")) == 0)
                    {
                        logFile.LogWrite("Invoice Number : " + str_CurLineInv + " ignored. Single Line invoice with Quantity=0 and Price=0 .", "SUCCESS");
                        lng_dtlCnt++;
                    }
                    else if (InvDRS.Length == 1 && double.Parse(dt.Rows[counter][18].ToString().Replace("\"", "")) == 0)
                    {
                        logFile.LogWrite("Invoice Number : " + str_CurLineInv + " ignored. Single Line invoice with Total=0 .", "SUCCESS");
                        lng_dtlCnt++;
                    }
                    else
                    {

                        dbl_InvTot = Convert.ToDouble(InvDRS.Sum(row => row.Field<double>("DblTotal")));
                        if (dbl_InvTot > 0)
                        {
                            //Invoice Records

                            for (int i = 0; i < InvDRS.Length; i++)
                            {
                                lng_dtlCnt++;
                                DataRow drscuccess = ValidatedTable.NewRow();
                                drscuccess = InvDRS[i];
                                ValidatedTable.ImportRow(drscuccess);   //only valid rows are copy in ValidatedTable Datatable

                            }//for (int i = 0; i < InvDRS.Length; i++)
                        }//if (dbl_InvTot > 0)
                        else
                        {
                            //credit note
                            for (int i = 0; i < InvDRS.Length; i++)
                            {
                                lng_dtlCnt++;
                                DataRow drscuccess = ValidatedCreditNotetbl.NewRow();
                                drscuccess = InvDRS[i];
                                ValidatedCreditNotetbl.ImportRow(drscuccess);   //only valid rows are copy in ValidatedTable Datatable

                            }//for (int i = 0; i < InvDRS.Length; i++)

                        }//if (dbl_InvTot <= 0)
                    }
                    
                    
                    counter = counter + (lng_dtlCnt - 1);
                    lng_dtlCnt = 0;
                }//for (long counter = 0; counter < dt.Rows.Count; counter++)
            }
            catch (Exception ex)
            { logFile.LogWrite("Error : SeparateCRandINV : Exception : " + ex.Message,"FAIL");  }
        }

        //S:GT:20221123: ReadInvoiceCrRecordsSeparately
        public void ReadInvoiceCrRecordsSeparately(DataRow row_dt)
        {
            string str_InvNo = row_dt[7].ToString();
            
            try
            {
                //Invoice Records
                if (row_dt[58].ToString() != "" && row_dt[21].ToString().Replace("\"", "").Trim() == "")
                {
                    DataRow drscuccess = ValidatedTable.NewRow();
                    drscuccess = row_dt;
                    ValidatedTable.ImportRow(drscuccess);   //only valid rows are copy in ValidatedTable Datatable
                }//if (row_dt[58].ToString() != "" && row_dt[21].ToString().Replace("\"", "").Trim() == "")
                //credit Note rows are validate
                else if (row_dt[21].ToString().Replace("\"", "").Trim() != "")
                {
                    DataRow drscuccess = ValidatedCreditNotetbl.NewRow();
                    drscuccess = row_dt;
                    ValidatedCreditNotetbl.ImportRow(drscuccess);   //only valid rows are copy in ValidatedTable Datatable
                }// if (row_dt[21].ToString().Replace("\"", "").Trim() != "")
                else
                {
                    logFile.LogWrite("Invalid Invoice Number : " + str_InvNo + ".", "FAIL");
                }
            }
            catch (Exception ex) { logFile.LogWrite("Error while validating import fiels : " + row_dt[7].ToString() + " : " + ex.Message, ERROR_TITLE); }
            finally { GC.Collect(); }
        }

        //E:GT:20221123: ReadInvoiceCrRecordsSeparately


        public string ValidateItem(string sItem, string invNo)
        {
            try
            {
                //sItem = "\"SSCAL10/20 - Scallop 10/20 Roe Off 10X1Kg  KILO\"";
                string[] Desc = sItem.Split(' ');
                string Item = Desc[0].ToString();
                Item = Item.Replace("\"", "").Trim();
                Item = Item.Replace("/", "").Trim();
                Item = Item.Replace("(", "").Trim();
                Item = Item.Replace(")", "").Trim();
                Item = Item.Replace("-", "").Trim();
                Item = Item.Replace(".", "").Trim();
                string str_Query;

                AccpacObj.mDBLinkCmpRE.OpenView("CS0120", out AccpacObj.objCSQry);
                AccpacObj.objCSQry.Cancel();
                str_Query = "SELECT ITEMNO FROM ICITEM (NOLOCK) WHERE ( ITEMNO='" + Item + "' OR FMTITEMNO='" + Item + "') AND INACTIVE=0";
                AccpacObj.objCSQry.Browse(str_Query, true);

                AccpacObj.objCSQry.InternalSet(256);
                if (AccpacObj.objCSQry.Fetch() == true)
                    return Item;
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                logFile.LogWrite("Invoice/Credit Note Number : " + invNo + " Invalid Item Number OR Item is Inactive " + sItem + ex.Message, "FAIL"); 
                return null;
            }
            finally { GC.Collect(); }
        }

        /// <summary>
        /// check customer is valid or not
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool ValidateCustomer(string customer,string invNo)
        {
            try
            {
                if (customer != "")
                {
                    customer = customer.Replace("\"", "").Trim();
                    string str_Query;

                    AccpacObj.mDBLinkCmpRE.OpenView("CS0120", out AccpacObj.objCSQry);
                    AccpacObj.objCSQry.Cancel();
                    str_Query = "SELECT IDCUST FROM ARCUS WHERE IDCUST = '" + customer + "'";

                    AccpacObj.objCSQry.Browse(str_Query, true);

                    AccpacObj.objCSQry.InternalSet(256);
                    if (AccpacObj.objCSQry.Fetch() == true)
                        return true;
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
                
            }
            catch (Exception ex)
            {
                logFile.LogWrite("Invoice Number : " + invNo + " Invalid Customer Number " + customer  + ex.Message, "FAIL");
                return false;
            }
            finally { GC.Collect(); }
        }

        /// <summary>
        /// Valid Location check
        /// </summary>
        /// <param name="PaymentTerms"></param>
        /// <returns></returns>
        public string ValidateLocation(string Loc,string invNo)
        {
            try
            {
                Loc = Loc.ToUpper();
                Loc = Loc.Replace("\"", "").Trim();
                List<String> lstlocations = new List<string> { "CV", "NB", "RT" };
                bool isValidLocation = lstlocations.Contains(Loc);
                if (isValidLocation)
                {
                    switch (Loc)
                    {
                        case "CV":
                            Loc = "1";
                            break;

                        case "NB":
                            Loc = "2";
                            break;

                        case "RT":
                            Loc = "3";
                            break;

                        default:
                            Loc = null;
                            break;
                    }
                    return Loc;
                }
                else { return null; }        
            }

            catch (Exception ex)
            {
                logFile.LogWrite("Invoice/Credit Number : " + invNo + " Invalid Location " + Location + ex.Message, "FAIL");
                return null;
            }
            finally { GC.Collect(); }
        }

        /// <summary>
        /// Valid payment returns Payterms
        /// </summary>
        /// <param name="PaymentTerms"></param>
        /// <returns></returns>
        public string ValiadtePaymentTerms(string PaymentTerms, string invNo)
        {
            PayTerms = string.Empty;
            PaymentTerms = PaymentTerms.ToUpper();
            PaymentTerms = PaymentTerms.Replace("\"", "").Trim();
            try
            {
                //S:GT:20221109: Changes in EOM Replacement logic as per initial requirement
                //if (PaymentTerms.Contains("EOM"))
                //{
                //    PayTerms = PaymentTerms.Replace("EOM", "");
                //    PayTerms = PayTerms.Replace(" ", "");
                //}
                if (PaymentTerms.Contains("DAYS EOM"))
                {
                    PayTerms = PaymentTerms.Replace("DAYS EOM", "EOM");
                    PayTerms = PayTerms.Replace(" ", "");
                }
                else
                    PayTerms = PaymentTerms.Replace(" ", "");

                //S:GT:20221109: Changes in EOM Replacement logic as per initial requirement


                if (PayTerms != "")
                {
                    string str_Query;

                    AccpacObj.mDBLinkCmpRE.OpenView("CS0120", out AccpacObj.objCSQry);
                    AccpacObj.objCSQry.Cancel();
                    str_Query = "SELECT CODETERM FROM ARRTA WHERE CODETERM = '" + PayTerms + "'";

                    AccpacObj.objCSQry.Browse(str_Query, true);

                    AccpacObj.objCSQry.InternalSet(256);
                    if (AccpacObj.objCSQry.Fetch() == true)
                        return PayTerms;
                    else
                    {
                        return null;
                    }
                }

                return PayTerms;
            }
            catch (Exception ex)
            {
                logFile.LogWrite("Invoice Number : " + invNo + " Invalid PaymentTerm " + PayTerms  + ex.Message, "FAIL");
                return null;
            }
        }

        //S:GT:KIRT:[]02212023:Check Locationwise Invoice Prefix
        public bool LocationwiseInvoice(string DocNo, string Loc)
        {
            try
            {
                if (Loc == "CV")
                {
                    if (DocNo.Length == 7 && DocNo.StartsWith("S"))
                    { return true; }
                    else return false;
                }
                else if (Loc == "NB")
                {
                    if (DocNo.Length== 7 && DocNo.StartsWith("SN"))
                    { return true; }
                    else return false;
                }
                else if (Loc == "RT")
                {
                    if ((DocNo.Length == 9 && DocNo.StartsWith("POS")) || (DocNo.Length==7 && DocNo.StartsWith("SN")))
                    { return true; }
                    else return false;
                }
                else
                {
                    logFile.LogWrite("Invoice Number : " + DocNo + " Locationwise Invoice number : Invalid Location : " + Loc, "FAIL");
                    return false;
                }
            }
            catch { logFile.LogWrite("Invoice Number : " + DocNo + " Locationwise Invoice number format : Location " + Loc, "FAIL"); return false; }
            finally { GC.Collect(); }
        }






        /// <summary>
        /// Validate invoice number for credit note
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public bool ValidateInvoiceNumber(string DocNo, string DocType)
        {
            try
            {
                if (DocNo != "")
                {
                    DocNo = DocNo.Replace("\"", "").Trim();
                    string str_Query;

                    AccpacObj.mDBLinkCmpRE.OpenView("CS0120", out AccpacObj.objCSQry);
                    AccpacObj.objCSQry.Cancel();
                    if(DocType=="INVOICE")
                        str_Query = "SELECT INVNUMBER FROM OEINVH (NOLOCK) WHERE INVNUMBER = '" + DocNo + "'";
                    if (DocType == "CREDIT")
                        str_Query = "SELECT CRDNUMBER FROM OECRDH (NOLOCK) WHERE CRDNUMBER = '" + DocNo + "'";
                    else
                        str_Query = "SELECT ORDNUMBER FROM OEORDH (NOLOCK) WHERE ORDNUMBER = '" + DocNo + "'";

                    AccpacObj.objCSQry.Browse(str_Query, true);
                    AccpacObj.objCSQry.InternalSet(256);
                    if (AccpacObj.objCSQry.Fetch() == true)
                        return true;
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                return false;
            }
            finally { GC.Collect(); }
        }

        //S:GT:VIB:10112021: Function for Getting Sage 300 system format date
        public string GetSage300FormattedDate(string p_str_Data)
        {
            try
            {
                if (p_str_Data != "")
                {
                    p_str_Data = p_str_Data.Replace("\"", "");
                    p_str_Data = p_str_Data.Replace("/", "");
                    //p_str_Data = DateTime.Parse(p_str_Data.Substring(4, 2) + "/" + p_str_Data.Substring(6, 2) + "/" + p_str_Data.Substring(0, 4)).ToShortDateString();

                    //DateTime dd = new DateTime(int.Parse(p_str_Data.Substring(0, 4)), int.Parse(p_str_Data.Substring(4, 2)), int.Parse(p_str_Data.Substring(6, 2)));
                    DateTime dd = new DateTime(int.Parse(p_str_Data.Substring(5, 4)), int.Parse(p_str_Data.Substring(3, 2)), int.Parse(p_str_Data.Substring(0, 3)));

                    //p_str_Data = dd.ToShortDateString();
                    p_str_Data = dd.ToString();
                }
                return p_str_Data;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally { GC.Collect(); }
        }
        //E:GT:VIB:10112021: Function for Getting Sage 300 system format date

        //S:GT:VIB:10112021: Function for Getting Sage 300 DB format date(YYYYMMDD) 
        public string GetSage300DBFormattedDate(string p_str_Data)
        {
            try
            {
                if (p_str_Data != "")
                {
                    DateTime dt;
                    dt = DateTime.Parse(p_str_Data);
                    p_str_Data = dt.Year.ToString("0000") + dt.Month.ToString("00") + dt.Day.ToString("00");
                }
                return p_str_Data;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally { GC.Collect(); }
        }
        //E:GT:VIB:10112021: Function for Getting Sage 300 DB format date(YYYYMMDD) 

        #endregion validations
    }
}
